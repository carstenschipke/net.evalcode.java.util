package net.evalcode.java.util.concurrent;


import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import com.google.common.collect.ForwardingQueue;


/**
 * UnboundBlockingQueue
 *
 * <p> A wrapper for {@link ConcurrentLinkedQueue} that blocks on {@link #poll()}
 * as long as the wrapped queue is empty.
 *
 * <p> Might be dangerous for production systems since an unbound queue implemented
 * as a work queue could lead to {@link OutOfMemoryError} if work is not getting done
 * as fast as required and the queue fills up over time.
 *
 * <p> Yet it can be useful under monitoring to evaluate the proper queue size for a
 * certain implementation and then replace this queue by an accordingly configured
 * bounded one.
 *
 * @author carsten.schipke@gmail.com
 * @see java.util.Queue
 * @see java.util.concurrent.ConcurrentLinkedQueue
 * @param <E> The type of elements held in this collection.
 */
public class UnboundBlockingQueue<E> extends ForwardingQueue<E>
{
  // MEMBERS
  private final ConcurrentLinkedQueue<E> queue=new ConcurrentLinkedQueue<E>();
  private final ReentrantLock lock=new ReentrantLock();
  private final Condition notEmpty=lock.newCondition();


  // OVERRIDES/IMPLEMENTS
  /**
   * Inserts the specified element at the tail of this queue.
   * As the queue is unbounded, this method will never return {@code false}.
   *
   * @param e the element to add
   * @return {@code true} (as specified by {@link Collection#add})
   * @throws NullPointerException if the specified element is null
   */
  @Override
  public boolean add(final E e)
  {
    return offer(e);
  }

  /**
   * Appends all of the elements in the specified collection to the end of
   * this queue, in the order that they are returned by the specified
   * collection's iterator.  Attempts to {@code addAll} of a queue to
   * itself result in {@code IllegalArgumentException}.
   *
   * @param c the elements to be inserted into this queue
   * @return {@code true} if this queue changed as a result of the call
   * @throws NullPointerException if the specified collection or any
   *         of its elements are null
   * @throws IllegalArgumentException if the collection is this queue
   */
  @Override
  public boolean addAll(final Collection<? extends E> c)
  {
    lock.lock();

    try
    {
      if(queue.isEmpty())
        notEmpty.signalAll();

      return super.addAll(c);
    }
    finally
    {
      lock.unlock();
    }
  }

  /**
   * Inserts the specified element at the tail of this queue.
   * As the queue is unbounded, this method will never return {@code false}.
   *
   * @param e the element to add
   * @return {@code true} (as specified by {@link Queue#offer})
   * @throws NullPointerException if the specified element is null
   */
  @Override
  public boolean offer(final E e)
  {
    lock.lock();

    try
    {
      if(queue.isEmpty())
        notEmpty.signalAll();

      return super.offer(e);
    }
    finally
    {
      lock.unlock();
    }
  }

  /**
   * Retrieves and removes the head of this queue, waiting if necessary
   * until an element becomes available.
   *
   * @return the head of this queue, or <tt>null</tt> if interrupted while waiting
   */
  @Override
  public E poll()
  {
    try
    {
      return pollImpl();
    }
    catch(final InterruptedException e)
    {
      Thread.currentThread().interrupt();
    }

    return null;
  }

  /**
   * Retrieves and removes the head of this queue, waiting up to the
   * specified wait time if necessary for an element to become available.
   *
   * @param timeout how long to wait before giving up, in units of
   *        <tt>unit</tt>
   * @param unit a <tt>TimeUnit</tt> determining how to interpret the
   *        <tt>timeout</tt> parameter
   * @return the head of this queue, or <tt>null</tt> if the
   *         specified waiting time elapses before an element is available
   * @throws InterruptedException if interrupted while waiting
   */
  public E poll(final long timeout, final TimeUnit unit) throws InterruptedException
  {
    lock.lockInterruptibly();

    try
    {
      while(queue.isEmpty())
      {
        if(!notEmpty.await(timeout, unit))
          break;
      }

      return super.poll();
    }
    finally
    {
      lock.unlock();
    }
  }


  // IMPLEMENTATION
  @Override
  protected Queue<E> delegate()
  {
    return queue;
  }

  E pollImpl() throws InterruptedException
  {
    lock.lockInterruptibly();

    try
    {
      while(queue.isEmpty())
        notEmpty.await();

      return super.poll();
    }
    finally
    {
      lock.unlock();
    }
  }
}
