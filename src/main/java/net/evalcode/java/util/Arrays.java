package net.evalcode.java.util;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Arrays
 *
 * @author carsten.schipke@gmail.com
 */
public final class Arrays
{
  // CONSTRUCTION
  private Arrays()
  {
    super();
  }


  // STATIC ACCESSORS
  public static int[] toArray(final Collection<Integer> collection)
  {
    return toArray(collection, false);
  }

  public static int[] toArray(final Collection<Integer> collection, final boolean nullToZero)
  {
    if(nullToZero)
      return toArrayImpl1(collection);

    return toArrayImpl0(collection);
  }


  // HELPERS
  static int[] toArrayImpl0(final Collection<Integer> collection)
  {
    final List<Integer> c=new ArrayList<>();

    for(Integer value : collection)
    {
      if(null!=value)
        c.add(value);
    }

    final int[] array=new int[c.size()];

    int i=0;
    for(final Integer value : c)
      array[i++]=value.intValue();

    return array;
  }

  static int[] toArrayImpl1(final Collection<Integer> collection)
  {
    final List<Integer> c=new ArrayList<>();

    for(Integer value : collection)
    {
      if(null==value)
        value=Integer.valueOf(0);

      c.add(value);
    }

    final int[] array=new int[c.size()];

    int i=0;
    for(final Integer value : c)
      array[i++]=value.intValue();

    return array;
  }
}
