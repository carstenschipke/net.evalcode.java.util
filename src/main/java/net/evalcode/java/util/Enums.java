package net.evalcode.java.util;


import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Enums
 *
 * @author carsten.schipke@gmail.com
 */
public final class Enums
{
  // CONSTRUCTION
  private Enums()
  {
    super();
  }


  // STATIC ACCESSORS
  public static <E> E forBit(final IndexedBitSet<E> indexedBitSet, final int bit)
  {
    return forBit(indexedBitSet, Integer.valueOf(bit));
  }

  /**
   * @throws NullPointerException If given bit is null.
   */
  public static <E> E forBit(final IndexedBitSet<E> indexedBitSet, final Integer bit)
  {
    return indexedBitSet.getIndex().get(bit);
  }

  /**
   * @throws NumberFormatException If given bit can not be parsed as an integer.
   */
  public static <E> E forBit(final IndexedBitSet<E> indexedBitSet, final String bit)
  {
    return forBit(indexedBitSet, Integer.valueOf(bit));
  }

  public static <E> Set<E> forBitMask(final IndexedBitSet<E> indexedBitSet, final int bitMask)
  {
    return forBitMask(indexedBitSet, Integer.valueOf(bitMask));
  }

  public static <E> Set<E> forBitMask(final IndexedBitSet<E> indexedBitSet, final Integer bitMask)
  {
    final Map<Integer, E> index=indexedBitSet.getIndex();
    final Set<E> values=new HashSet<>();

    for(final Integer bit : index.keySet())
    {
      if(0<(bitMask.intValue()&bit.intValue()))
        values.add(index.get(bit));
    }

    return values;
  }

  /**
   * @throws NumberFormatException If given bitMask can not be parsed as an integer.
   */
  public static <E> Set<E> forBitMask(final IndexedBitSet<E> indexedBitSet, final String bitMask)
  {
    return forBitMask(indexedBitSet, Integer.valueOf(bitMask));
  }


  /**
   * IndexedBitset
   *
   * @author carsten.schipke@gmail.com
   */
  public interface IndexedBitSet<E>
  {
    // ACCESSORS/MUTATORS
    Map<Integer, E> getIndex();
  }
}
