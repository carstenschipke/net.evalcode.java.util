package net.evalcode.java.util.concurrent;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import net.evalcode.java.util.concurrent.UnboundBlockingQueue;
import org.junit.Test;
import com.google.common.collect.Lists;


/**
 * Test {@link UnboundBlockingQueue}
 *
 * @author carsten.schipke@gmail.com
 */
public class UnboundBlockingQueueTest
{
  // PREDEFINED PROPERTIES
  private static final long DELAY_FIRST_POLL=10L;
  private static final long DELAY_SECOND_POLL=20L;
  private static final long DELAY_OFFER=30L;
  private static final long TIMEOUT_TEST=100L;
  private static final long TIMEOUT_TEST_POLL=1000L;
  private static final long WAIT_TEST_POLL=100L;


  // TESTS
  @Test
  public void testConcurrentPollAndOffer() throws InterruptedException
  {
    final UnboundBlockingQueue<String> queue=new UnboundBlockingQueue<String>();
    final CountDownLatch done0=new CountDownLatch(1);
    final CountDownLatch done1=new CountDownLatch(1);
    final AtomicInteger count=new AtomicInteger(0);
    final AtomicBoolean done=new AtomicBoolean(false);

    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
      @Override
      public void run()
      {
        queue.poll();

        count.incrementAndGet();
        done0.countDown();
        done.set(true);
      }
    }, DELAY_FIRST_POLL, TimeUnit.MILLISECONDS);

    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
      @Override
      public void run()
      {
        queue.poll();

        count.incrementAndGet();
        done1.countDown();
        done.set(true);
      }
    }, DELAY_SECOND_POLL, TimeUnit.MILLISECONDS);


    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
      @Override
      public void run()
      {
        queue.offer("foo");
      }
    }, DELAY_OFFER, TimeUnit.MILLISECONDS);

    done0.await(TIMEOUT_TEST, TimeUnit.MILLISECONDS);
    done1.await(TIMEOUT_TEST, TimeUnit.MILLISECONDS);

    assertTrue(done.get());
    assertFalse(2==count.get());
  }

  @Test
  public void testPoll() throws InterruptedException
  {
    final CountDownLatch done=new CountDownLatch(1);
    final UnboundBlockingQueue<String> queue=new UnboundBlockingQueue<String>();
    queue.addAll(Lists.asList("foo", new String[] {"bar"}));

    Executors.newSingleThreadExecutor().execute(new Runnable() {
      @Override
      public void run()
      {
        queue.poll();
        queue.poll();
        queue.poll();

        done.countDown();
      }
    });

    assertFalse(done.await(TIMEOUT_TEST, TimeUnit.MILLISECONDS));
  }

  @Test
  public void testPollInterrupted() throws InterruptedException
  {
    final CountDownLatch done=new CountDownLatch(1);
    final UnboundBlockingQueue<String> queue=new UnboundBlockingQueue<String>();
    queue.addAll(Lists.asList("foo", new String[] {"bar"}));

    Executors.newSingleThreadExecutor().execute(new Runnable() {
      @Override
      public void run()
      {
        queue.poll();
        queue.poll();

        Thread.currentThread().interrupt();
        assertNull(queue.poll());
        assertTrue(Thread.interrupted());

        done.countDown();
      }
    });

    done.await();
  }

  @Test(timeout=TIMEOUT_TEST_POLL)
  public void testPollTimeout() throws InterruptedException
  {
    final CountDownLatch done=new CountDownLatch(1);
    final UnboundBlockingQueue<String> queue=new UnboundBlockingQueue<String>();
    queue.add("foo");
    queue.add("bar");

    Executors.newSingleThreadExecutor().execute(new Runnable() {
      @Override
      public void run()
      {
        queue.poll();
        queue.poll();

        try
        {
          queue.poll(WAIT_TEST_POLL, TimeUnit.MILLISECONDS);
        }
        catch(final InterruptedException e)
        {
          Thread.currentThread().interrupt();
        }

        done.countDown();
      }
    });

    done.await();

    assertFalse(Thread.interrupted());
  }
}
