package net.evalcode.java.util;


import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import net.evalcode.java.util.Arrays;
import org.junit.Test;


/**
 * Test {@Arrays}
 *
 * @author carsten.schipke@gmail.com
 */
public class ArraysTest
{
  // TESTS
  @Test
  public void testToArray0()
  {
    final List<Integer> collection=new ArrayList<Integer>() {{
        add(Integer.valueOf(1));
        add(null);
        add(Integer.valueOf(3));
      }
      private static final long serialVersionUID=1L;
    };

    final int[] array=Arrays.toArray(collection);

    assertEquals(1, array[0]);
    assertEquals(3, array[1]);
  }

  @Test
  public void testToArray1()
  {
    final List<Integer> collection=new ArrayList<Integer>() {{
        add(Integer.valueOf(1));
        add(null);
        add(Integer.valueOf(3));
      }
      private static final long serialVersionUID=1L;
    };

    final int[] array=Arrays.toArray(collection, true);

    assertEquals(1, array[0]);
    assertEquals(0, array[1]);
    assertEquals(3, array[2]);
  }
}
