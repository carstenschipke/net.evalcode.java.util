package net.evalcode.java.util;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.evalcode.java.util.Enums;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Test {@link Enums}
 *
 * @author carsten.schipke@gmail.com
 */
public class EnumsTest
{
  // TESTS
  @Test
  public void testForBit()
  {
    assertEquals(Foo.ONE, Enums.forBit(Foo.ONE, 1));
    assertEquals(Foo.TWO, Enums.forBit(Foo.ONE, 2));
    assertEquals(Foo.FOUR, Enums.forBit(Foo.ONE, 4));
    assertEquals(Foo.EIGHT, Enums.forBit(Foo.ONE, 8));
    assertEquals(Foo.SIXTEEN, Enums.forBit(Foo.ONE, 16));

    assertEquals(Foo.ONE, Enums.forBit(Foo.ONE, "1"));
    assertEquals(Foo.TWO, Enums.forBit(Foo.ONE, "2"));
    assertEquals(Foo.FOUR, Enums.forBit(Foo.ONE, "4"));
    assertEquals(Foo.EIGHT, Enums.forBit(Foo.ONE, "8"));
    assertEquals(Foo.SIXTEEN, Enums.forBit(Foo.ONE, "16"));
  }

  @Test
  public void testForBitMask()
  {
    final Set<Foo> set0=Enums.forBitMask(Foo.ONE, 31);

    assertTrue(set0.contains(Foo.ONE));
    assertTrue(set0.contains(Foo.TWO));
    assertTrue(set0.contains(Foo.FOUR));
    assertTrue(set0.contains(Foo.EIGHT));
    assertTrue(set0.contains(Foo.SIXTEEN));

    final Set<Foo> set1=Enums.forBitMask(Foo.ONE, 21);

    assertTrue(set1.contains(Foo.ONE));
    assertTrue(set1.contains(Foo.FOUR));
    assertTrue(set1.contains(Foo.SIXTEEN));

    final Set<Foo> set2=Enums.forBitMask(Foo.ONE, "11");

    assertTrue(set2.contains(Foo.ONE));
    assertTrue(set2.contains(Foo.TWO));
    assertTrue(set2.contains(Foo.EIGHT));

    final Set<Foo> set4=Enums.forBitMask(Foo.ONE, "3");

    assertTrue(set4.contains(Foo.ONE));
    assertTrue(set4.contains(Foo.TWO));
  }


  /**
   * Foo
   *
   * @author carsten.schipke@gmail.com
   */
  @Ignore
  static enum Foo implements Enums.IndexedBitSet<Foo>
  {
    // PREDEFINED VALUES
    ONE(1),
    TWO(2),
    FOUR(4),
    EIGHT(8),
    SIXTEEN(16);


    // CONSTRUCTION
    Foo(final int bit)
    {
      this.bit=bit;
    }


    // OVERRIDES/IMPLEMENTS
    @Override
    public Map<Integer, Foo> getIndex()
    {
      return INDEX;
    }


    // MEMBERS
    final int bit;

    static final Map<Integer, Foo> INDEX=new HashMap<Integer, Foo>() {{
        put(Integer.valueOf(ONE.bit), ONE);
        put(Integer.valueOf(TWO.bit), TWO);
        put(Integer.valueOf(FOUR.bit), FOUR);
        put(Integer.valueOf(EIGHT.bit), EIGHT);
        put(Integer.valueOf(SIXTEEN.bit), SIXTEEN);
      }
      private static final long serialVersionUID=1L;
    };
  }
}
